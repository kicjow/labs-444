﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4.Main
{
    public class Program
    {
        static void Main(string[] args)
        {
            var p1 = System.IO.Directory.GetCurrentDirectory();
            var p2 = System.AppDomain.CurrentDomain.BaseDirectory;
            var p3 = Environment.CurrentDirectory;
            Console.WriteLine(p1);
            Console.WriteLine(p2);
            Console.WriteLine(p3);
            Console.ReadKey();
        }
    }
}
