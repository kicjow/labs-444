﻿using System;

namespace Lab4.Main
{
    public struct LabDescriptor
    {
        #region definitions

        public delegate object GetInstance(object component2);
        public delegate void RegisterComponentInContainer(object container, object component);
        public delegate bool AreDependenciesResolved(object container);

        #endregion

        #region P1

        public static Type Component1 = typeof(void);
        public static Type Component2 = typeof(void);

        public static Type RequiredInterface = typeof(void);

        public static GetInstance GetInstanceOfRequiredInterface = (component2) => null;
        
        #endregion

        #region P2

        public static Type Container = typeof(void);

        public static RegisterComponentInContainer RegisterComponent = (container, component) => { };

        public static AreDependenciesResolved ResolvedDependencied = (container) => false;

        #endregion

        #region P3

        public static Type Component2B = typeof(void);

        #endregion
    }
}
